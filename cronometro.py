from tkinter import *
from PIL import Image
from PIL import ImageTk

cronometro=0
segundos=0
minutos=0
horas=0
width=100
height=100

root=Tk()
root.title("cronometro")
root.geometry('1600x300')

def crono():
    global cronometro,segundos,minutos,horas
    segundos += 1
    seg['text'] = str(segundos)
    cronometro=seg.after(100,crono)
    
    if (segundos>=59):
        segundos=0
        minutos +=1
        minu['text'] = str(minutos)
        if(minutos>=59):
            minutos=0
            horas +=1
            hor['text'] = str(horas)
def pause():
    global cronometro
    seg.after_cancel(cronometro)

def reiniciar():
    global segundos,minutos,horas
    segundos=0
    minutos=0
    horas=0

img=Image.open("/Users/yeny/Downloads/logousa.png") #cambiar direccion profe
img= img.resize((width,height))
imagen=ImageTk.PhotoImage(img)
b=Label(root,image=imagen).place(x=960,y=50)

titulo=Label(root,text="cronometro",font=("Arial Bold",18),fg="black")
titulo.pack()

seg=Label(root,text="00")
seg.place(x=230,y=90)
seg.config(bg='white',width=6, height=2)

puntos=Label(root,text=":")
puntos.place(x=210,y=90)
puntos.config(bg='white',width=2, height=2)

minu=Label(root,text="00")
minu.place(x=160,y=90)
minu.config(bg='white',width=6, height=2)

puntos1=Label(root,text=":")
puntos1.place(x=150,y=90)
puntos1.config(bg='white',width=2, height=2)

hor=Label(root,text="00")
hor.place(x=100,y=90)
hor.config(bg='white',width=6, height=2)

start=Button(root,text="iniciar",command=crono)
start.place(x=10,y=140)
start.config(width=15, height=2)

pausa=Button(root,text="pausar",command=pause)
pausa.place(x=140,y=140)
pausa.config(width=15, height=2)

reiniciar=Button(root,text="reiniciar",command=reiniciar)
reiniciar.place(x=10,y=200)
reiniciar.config(width=15, height=2)

salir=Button(root,text="salir",command=root.destroy)
salir.place(x=140,y=200)
salir.config(width=15, height=2)

root.mainloop()
