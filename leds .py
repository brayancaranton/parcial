from pyfirmata import Arduino, util
from PIL import Image
from PIL import ImageTk
from tkinter import *
import time
placa = Arduino ('/dev/cu.usbmodem1421')#cambiar puerto profe
it = util.Iterator(placa)
canvas_width =  600
canvas_height = 800
sizex_circ=60
sizey_circ=60
width = 150
height = 100
img=Image.open("/Users/yeny/Downloads/logousa.png") #cambiar direccion profe
img= img.resize((width,height))
imagen=ImageTk.PhotoImage(img)
b=Label(ventana,image=imagen).place(x=960,y=50)
#inicio el iteratodr
it.start()
L1= placa.get_pin('d:13:o')
L2= placa.get_pin('d:12:o')
L3= placa.get_pin('d:11:o')
L4= placa.get_pin('d:10:o')
L5= placa.get_pin('d:9:o')
L6= placa.get_pin('d:8:o') 

ventana = Tk()
ventana.geometry('1200x800')
ventana.configure(bg = 'white')
ventana.title("CONTROL DE LEDS")

draw = Canvas(ventana, width=canvas_width, height=canvas_height)
draw.place(x = 10,y = 50)

L1_draw=draw.create_oval(10,10,10+sizex_circ,10+sizey_circ,fill="white")
L2_draw=draw.create_oval(75,10,75+sizex_circ,10+sizey_circ,fill="white")
L3_draw=draw.create_oval(140,10,140+sizex_circ,10+sizey_circ,fill="white")
L4_draw=draw.create_oval(210,10,210+sizex_circ,10+sizey_circ,fill="white")
L5_draw=draw.create_oval(275,10,275+sizex_circ,10+sizey_circ,fill="white")
L6_draw=draw.create_oval(340,10,340+sizex_circ,10+sizey_circ,fill="white")

def encender(valor):
    if (int(valor)>0):
        L1.write(1)
        L1label['text'] = "LED 1"
        L1label.place(x = 35,y =30)
        draw.itemconfig(L1_draw, fill="yellow")
    else:
        L1.write(0)
        L1label.place_forget()
        draw.itemconfig(L1_draw, fill="white")
    if(int(valor)>15):
        L2.write(1)
        L2label['text'] = "LED 2"
        L2label.place(x = 100,y =30)
        draw.itemconfig(L2_draw, fill="red")
    else:
        L2.write(0)
        L2label.place_forget()
        draw.itemconfig(L2_draw, fill="white")
    if (int(valor)>30):
        L3.write(1)
        L3label['text'] = "LED 3"
        L3label.place(x = 165,y =30)
        draw.itemconfig(L3_draw, fill="red")
    else:
        L3.write(0)
        L3label.place_forget()
        draw.itemconfig(L3_draw, fill="white")
    if(int(valor)>45):
        L4.write(1)
        L4label['text'] = "LED 4"
        L4label.place(x = 235,y =30)
        draw.itemconfig(L4_draw, fill="yellow")
    else:
        L4.write(0)
        L4label.place_forget()
        draw.itemconfig(L4_draw, fill="white")
    if (int(valor)>60):
        L5.write(1)
        L5label['text'] = "LED 5"
        L5label.place(x = 300,y =30)
        draw.itemconfig(L5_draw, fill="green")
    else:
        L5.write(0)
        L5label.place_forget()
        draw.itemconfig(L5_draw, fill="white")
    if(int(valor)>75):
        L6.write(1)
        L6label['text'] = "LED 6"
        L6label.place(x = 365,y =30)
        draw.itemconfig(L6_draw, fill="blue")
    else:
        L6.write(0)
        L6label.place_forget()
        draw.itemconfig(L6_draw, fill="white")
        
lum = Scale(ventana, 
            command=encender, 
            from_= 0, 
            to = 100, 
            orient = HORIZONTAL, 
            length = 390, 
            label = "INTENSIDAD",
            bg = 'cadet blue1',
            font=("Arial Bold", 15),
            fg="white"
            )
lum.grid(padx=20, pady=150,column=0, row=1)
Label(ventana, text="CONTROL DE LEDS").place(x=550, y=10)
L1label= Label(ventana,bg="YELLOW", font=("Arial Bold", 8), fg="black")
L2label= Label(ventana,bg="RED", font=("Arial Bold", 8), fg="white")
L3label= Label(ventana,bg="RED", font=("Arial Bold", 8), fg="white")
L4label= Label(ventana,bg="YELLOW", font=("Arial Bold", 8), fg="black")
L5label= Label(ventana,bg="GREEN", font=("Arial Bold", 8), fg="white")
L6label= Label(ventana,bg="BLUE", font=("Arial Bold", 8), fg="white")
ventana.mainloop()
   
